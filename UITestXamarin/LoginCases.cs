﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace UITestXamarin
{
    class LoginCases : Base
    {
        public LoginCases(IApp app) : base(app)
        {

        }

        public Boolean ValidCredentials()
        {
            Query loginButton = c => c.Marked("log_in_button");
            Query userText = c => c.Marked("user");
            Query buttonUser = c => c.Marked("authenticate");
            Query passwordText = c => c.Marked("password");
            Query homeboard = c => c.Marked("boards_container");

            this.Tap(loginButton);
            this.WaitForElement(userText);
            this.EnterText(userText, "josuem@proyecto.com");
            this.Tap(buttonUser);
            this.WaitForElement(passwordText);
            this.EnterText(passwordText, "123456789");
            this.Tap(buttonUser);
            this.WaitForElement(homeboard);

            return true;
        }

        public Boolean InvalidCredentials()
        {
            Query loginButton = c => c.Marked("log_in_button");
            Query userText = c => c.Marked("user");
            Query buttonUser = c => c.Marked("authenticate");
            Query passwordText = c => c.Marked("password");
            Query alertView = c => c.Marked("alertTitle");

            this.Tap(loginButton);
            this.WaitForElement(userText);
            this.EnterText(userText, "josuem@incorrect.com");
            this.Tap(buttonUser);
            this.WaitForElement(passwordText);
            this.EnterText(passwordText, "987654321");
            this.Tap(buttonUser);
            this.WaitForElement(alertView);

            return true;
        }

        public Boolean OnlyInvalidPassword()
        {
            Query loginButton = c => c.Marked("log_in_button");
            Query userText = c => c.Marked("user");
            Query buttonUser = c => c.Marked("authenticate");
            Query passwordText = c => c.Marked("password");
            Query alertView = c => c.Marked("alertTitle");

            this.Tap(loginButton);
            this.WaitForElement(userText);
            this.EnterText(userText, "josuem@proyecto.com.com");
            this.Tap(buttonUser);
            this.WaitForElement(passwordText);
            this.EnterText(passwordText, "987654321");
            this.Tap(buttonUser);
            this.WaitForElement(alertView);

            return true;
        }

    }
}
