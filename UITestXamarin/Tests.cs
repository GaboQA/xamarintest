﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestXamarin
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;
        LoginCases login;
        RegisterCases register;
        Update update;
        TestDelete delete;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            this.app = ConfigureApp
            .Android
            .ApkFile(@"./UITestXamarin/bin/Debug/Trello.apk")
            .StartApp();
            this.update = new Update(this.app);
            this.register = new RegisterCases(this.app);
            this.login = new LoginCases(this.app);
            this.delete = new TestDelete(this.app);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        //[Test]
        //public void WelcomeTextIsDisplayed()
        //{
        //    //app.Repl();
        //    Assert.IsTrue(true);
        //}

        [Test]
        public void A_LoginValidCredentials()
        {
          Boolean result = this.login.ValidCredentials();
          Assert.IsTrue(result);
        }

        [Test]
        public void B_LoginInvalidCredentials()
        {
           Boolean result = this.login.InvalidCredentials();
           Assert.IsTrue(result);
        }

        [Test]
        public void C_LoginOnlyInvalidPassword()
        {
            Boolean result = this.login.OnlyInvalidPassword();
            Assert.IsTrue(result);
        }

        ////----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void D_RegisterEmptySpace()
        {
           Boolean result = this.register.EmptySpace();
           Assert.IsTrue(result);
        }

        [Test]
        public void E_RegisterSymbols()
        {
            Boolean result = this.register.Symbols();
            Assert.IsTrue(result);
        }
        [Test]
        public void F_RegistersSuccess()
        {
              Boolean result = this.register.NormalsWords();
              Assert.IsTrue(result);
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Update
          [Test]
          public void G_TestUpdateWriting()
          {
            Boolean result = this.update.UpdateWriting();
            Assert.IsTrue(result);
          }

        [Test]
        public void H_TestUpdateSymbols()
        {
             Boolean result = this.update.UpdateSymbols();
             Assert.IsTrue(result);
        }
        [Test]
        public void I_TestUpdateEmpty()
        {
            Boolean result = this.update.UpdateEmpty();
            Assert.IsTrue(result);
        }

        ////---------------------
        [Test]
        public void J_TestDeleteCard()
        {
          Boolean result = this.delete.DeleteCard();
          Assert.IsTrue(result);
        }

    }
}
